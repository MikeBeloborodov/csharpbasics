﻿using System;

namespace CSharpBasics.Utilities
{
	public class ArrayHelper
	{
		/// <summary>
		/// Вычисляет сумму неотрицательных элементов в одномерном массиве
		/// </summary>
		/// <param name="numbers">Одномерный массив чисел</param>
		/// <returns>Сумма неотрицательных элементов массива</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfPositiveElements(int[] numbers)
		{
			// first we check if numbers is null
			if (numbers == null)
            {
				throw new ArgumentNullException("numbers", "numbers is null");
            }

			// let's create a variable for the sum
			float sum = 0;

			// now let's use for loop to check all the numbers
			// and if a number is > 0 we are going to add it to the sum
			for (int i = 0; i < numbers.Length; i++)
            {
				if (numbers[i] > 0)
                {
					sum += numbers[i];
                }
            }

			return sum;
		}

		/// <summary>
		/// Заменяет все отрицательные элементы в трёхмерном массиве на нули
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public void ReplaceNegativeElementsBy0(int[,,] numbers)
		{
			// first we check if numbers is null
			if (numbers == null)
			{
				throw new ArgumentNullException("numbers", "numbers is null");
			}

			// now let's crete a nested loop to check each element of this 3d array
			// if the element is < 0 we're going to change it to 0
			for (int i = 0; i < numbers.GetLength(0); i++)
            {
				for (int j = 0; j < numbers.GetLength(1); j++)
                {
					for (int k = 0; k < numbers.GetLength(2); k++)
                    {
						if (numbers[i,j,k] < 0)
                        {
							numbers[i, j, k] = 0;
                        }
                    }
                }
            }
		}

		/// <summary>
		/// Вычисляет сумму элементов двумерного массива <see cref="numbers"/>,
		/// которые находятся на чётных позициях ([1,1], [2,4] и т.д.)
		/// </summary>
		/// <param name="numbers">Двумерный массив целых чисел</param>
		/// <returns>Сумма элементов на четных позициях</returns>
		/// <exception cref="ArgumentNullException"> Выбрасывается, если <see cref="numbers"/> равен null</exception>
		public float CalcSumOfElementsOnEvenPositions(int[,] numbers)
		{
			// first we check if numbers is null
			if (numbers == null)
			{
				throw new ArgumentNullException("numbers", "numbers is null");
			}

			// let's create a variable for the sum
			float sum = 0;

			// now we're going to check every position in numbers
			// and if the sum of counters i and j is even - then this position is even 
			// and we're going to add this element value to the sum
			for (int i = 0; i < numbers.GetLength(0); i++)
            {
				for (int j = 0; j < numbers.GetLength(1); j++)
                {
					if ( (i + j) % 2 == 0)
                    {
						sum += numbers[i, j];
                    }
                }
            }

			return sum;
		}

		/// <summary>
		/// Фильтрует массив <see cref="numbers"/> таким образом, чтобы на выходе остались только числа, содержащие цифру <see cref="filter"/>
		/// </summary>
		/// <param name="numbers">Массив целых чисел</param>
		/// <param name="filter">Цифра для фильтрации массива <see cref="numbers"/></param>
		/// <returns></returns>
		public int[] FilterArrayByDigit(int[] numbers, byte filter)
		{
			// let's check if numbers are null
			// if it is we return an empty array
			if(numbers == null)
            {
				return new int[0];
			}

			// let's make a counter that will count all elements which contain filter number
			// we will need it later to create an array with exact amount of elements
			int counter = 0;

			// counting all filtered elements in numbers array
			foreach (var item in numbers)
			{
				if (item.ToString().Contains(filter.ToString()))
				{
					counter++;
				}
			}

			// now let's create a filteredArray using our counter to specify the exact size of an array
			int[] filteredArray = new int[counter];

			// let's assign counter back to 0 and count everything again
			// but this time we will add filtered elements to our filteredArray
			counter = 0;

			foreach (var item in numbers)
			{
				if (item.ToString().Contains(filter.ToString()))
				{
					filteredArray[counter] = item;
					counter++;
				}
			}

			return filteredArray;
		}
	}
}