﻿using System;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
		/// <summary>
		/// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
		/// </summary>
		/// <param name="number">Натуральное число</param>
		/// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
		/// <returns>Вычисленная сумма</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
		/// а также если массив <see cref="divisors"/> пустой
		/// </exception>
		public float CalcSumOfDivisors(int number, params int[] divisors)
		{
			// first we check if number is out of range
			if (number <= 0)
            {
				throw new ArgumentOutOfRangeException("number", "number is out of range");
            }

			// then we check if divisors are null or empty
			if (divisors == null || divisors.Length == 0)
            {
				throw new ArgumentOutOfRangeException("divisors", "divisors are null or empty");
			}

			// and if they're out of range
			for (int i = 0; i < divisors.Length; i++)
            {
				if (divisors[i] <= 0)	
                {
					throw new ArgumentOutOfRangeException("divisors", "divisors are out of range");
				}
			}

			// now let's create a variable for the sum
			float sum = 0;

			// and finally we calculate the sum by using a for loop
			// we start from 1 and not 0, because we use i to check all natural numbers from 1 to number
			// and 0 is not a natural number
			for (int i = 1; i < number; i++)
            {
				// here we start from 0 because we use j to go trough all members of divisors array
				for (int j = 0; j < divisors.Length; j++)
                {
					if (i % divisors[j] == 0)
                    {
						sum += i;
                    }
                }
            }

			return sum;
		}

		/// <summary>
		/// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
		/// </summary>
		/// <param name="number">Исходное число</param>
		/// <returns>Ближайшее наибольшее целое</returns>
		public long FindNextBiggerNumber(int number)
		{
			// first we check if number is out of range
			if (number <= 0)
			{
				throw new ArgumentOutOfRangeException("number", "number is out of range");
			}

			// then we put every digit in a char array
			char[] singleDigits = number.ToString().ToCharArray();

			// if there is only 1 digit we can throw an exeption, because there is no way we can make it bigger
			// without changing digits
			if (singleDigits.Length == 1)
			{
				throw new InvalidOperationException();
			}

			// now we start to look for the smallest change that we can do to make this number bigger
			// we start from the end of the array
			for (int i = singleDigits.Length - 1; i > 0; i--)
			{
				// if we find that single smallest change we use buffer to switch two digits
				if (singleDigits[i] > singleDigits[i - 1])
				{
					char buffer = singleDigits[i - 1];
					singleDigits[i - 1] = singleDigits[i];
					singleDigits[i] = buffer;
					// if this change happened at the end of the number
					// for example if we have number 623 and we change it to 632 there is no way we can make it bigger without changing digits
					// it means that it's going to be the smallest change and we can break the loop and return the value
					if (i + 1 == singleDigits.Length)
					{
						break;
					}
					// but if this change happened somewhere later - we can check if we can make this change even smaller
					// by rearranging other digits from smallest to biggest 
					else
					{
						for (int j = i; j < singleDigits.Length; j++)
						{
							for (int k = j + 1; k < singleDigits.Length; k++)
							{
								if (singleDigits[j] > singleDigits[k])
								{
									buffer = singleDigits[j];
									singleDigits[j] = singleDigits[k];
									singleDigits[k] = buffer;
								}
							}
						}
						// break out of the loop after all rearrangements
						break;
					}
				}
			}
			// now let's use string builder to store our char array because we have to parse it into long later
			StringBuilder resultString = new StringBuilder();

			for (int i = 0; i < singleDigits.Length; i++)
			{
				resultString.Append(singleDigits[i]);
			}

			// now let's parse this string builder into string and then into long
			long biggerNumber = long.Parse(resultString.ToString());

			// and finally we check if our new number even changed at all
			// if it hasn't changed we can throw an exception
			if (biggerNumber == number)
			{
				throw new InvalidOperationException();
			}

			return biggerNumber;
		}
	}
}