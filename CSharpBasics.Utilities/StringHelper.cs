﻿using System;
using System.Text;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			// first we check if inputString is null
			if (inputString == null)
			{
				return 0;
			}

			// then we split inputString and remove all spaces
			string[] splitString = inputString.Split(' ');

			// now we're going to count all valid words which contain letters from "а" to "я" and a special case "ё"
			// by using wordCounter and foreach loop
			// if there's at least one valid letter in a word - we're going to add it as a word
			int wordCounter = 0;

			foreach (var item in splitString)
			{
				for (int i = 0; i < item.Length; i++)
				{
					if ((char.ToLower(item[i]) >= 'а' && char.ToLower(item[i]) <= 'я') || char.ToLower(item[i]) == 'ё')
					{
						wordCounter++;
						break;
					}
				}
			}

			// if there're no valid words we just return 0
			if (wordCounter == 0)
			{
				return 0;
			}

			// now let's count all letters in all words
			// we're going to use foreach loop and check for letters from "a" to "я"
			// and because "ё" is a special case, we're going to check for it as well
			int letterCounter = 0;

			foreach (var item in splitString)
			{
				for (int i = 0; i < item.Length; i++)
				{
					if ((char.ToLower(item[i]) >= 'а' && char.ToLower(item[i]) <= 'я') || char.ToLower(item[i]) == 'ё')
					{
						letterCounter++;
					}
				}
			}

			// and finally we just divide letters by words and return the average word length
			return letterCounter / wordCounter;
		}

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			// first we check if toDuplicate is null or empty, if so we return original string
			if (toDuplicate == null || toDuplicate.Length == 0)
            {
				return original;
            }

			// then we check if original is null
			if (original == null)
            {
				return null;
            }

			// now let's create 2 char arrays, one is made from toDuiplicate, we're going to use it to compare
			// the other char array is made from original string
			char[] splitDuplicate = toDuplicate.ToLower().ToCharArray();

			char[] splitOriginal = original.ToCharArray();

			// also we're going to use string builder to save memory
			StringBuilder resultString = new StringBuilder();

			// now in a foreach loop we're going to check if a letter from original string is equal to a letter from duplicate
			// if so we're going to add it twice to the result string
			// if not, we're only adding original letter
			foreach (var letter in splitOriginal)
            {
				for (int i = 0; i < splitDuplicate.Length; i++)
                {
					if (char.ToLower(letter) == splitDuplicate[i])
					{
						resultString.Append(letter);
						resultString.Append(letter);
						break;
					}
					else if(i + 1 == splitDuplicate.Length)
                    {
						resultString.Append(letter);
					}
				}
			}

			//let's stringify our result and return it
			return resultString.ToString();
		}
	}
}